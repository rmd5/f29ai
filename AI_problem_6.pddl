﻿; F29AI Coursework 1
; Knowledge representation and automated planning
; Rory Dobson, rmd5

(define (problem space)
    (:domain space)
    
    (:objects
    
        bridge                                              ; sections on ship
        launch_bay 
        science_lab 
        engineering 
        outside       
        
        captain                                             ; personnel
        science_officer 
        navigator                       
        MAV_operator                                        ; engineers
        control_operator 
        launch_controller
        engineer
        
        sol		                                            ; space regions
        proxima_centauri - planet_sys
        chalawan - planet_sys
        sirius - planet_sys
        betelgeuse - planet_sys
        rigel - planet_sys
        fomalhaut - planet_sys
        vega - planet_sys
        helvetios - planet_sys
        kepler - planet_sys
        
        earth                                               ; planets in sol
        venus 
        mars 
        jupiter 
        saturn 
        mercury 
        uranus 
        neptune  
        
        proxima_b                                           ; planet in proxima_centauri
        
        taphao_thong                                        ; planets in chalawan
        taphao_kaew
        ursae_majoris_d
        
        dagon                                               ; planet in fomalhaut
        
        dimidium                                            ; planet in helvetios
        
        kepler_a                                            ; planets in kepler 62
        kepler_b
        kepler_c
        kepler_d
        kepler_e
        
        deep_space                                          ; deep space in any star_system
        
        nebula                                              ; properties of region
        asteroid_belt
        planet
        
        plasma_sirius                                       ; retrieved from nebula
        plasma_proxima_centauri
        plasma_helvetios
        plasma_kepler
        plasma_betelgeuse

	    studied
        
        damaged                                             ; levels of damage
        not_damaged                              
        destroyed
        
        probe_1 probe_2 probe_3 probe_4 probe_5 - probe
        lander_1 lander_2 lander_3 lander_4 lander_5 lander_6 lander_7 lander_8 - lander
        MAV 
        radiation_shield
        antenna_1 antenna_2 - antenna                       ; each lander has these inbuilt
        
        found                                               ; whether optimal touchdown exists or not
        not_found
        
        high_radiation                                      ; radiation level of planets
        low_radiation
        
        visit                                               ; visit to star_system to store in central_computer
        
        ; objects created for additional requirements
        life                                                ; life found on planets
        planet_resources                                    ; resources obtained through alliance

        war                                                 ; either start war or create alliance depending on the planet disposition
	    alliance

	    hostile                                             ; disposition of life on planet
	    friendly
    )
    
    (:init
        (personnel captain)                                 ; assign personnel
        (personnel MAV_operator)
        (personnel control_operator)
        (personnel launch_controller)
        (personnel science_officer)
        (personnel navigator)
        (personnel engineer)
        
        (section_location bridge)                           ; assign ship sections
        (section_location launch_bay)
        (section_location science_lab)
        (section_location engineering)
        
        (planet earth)                                      ; initialise planets
        (planet venus)
        (planet mars)
        (planet jupiter)
        (planet saturn)
        (planet mercury)
        (planet uranus)
        (planet neptune)
        (planet proxima_b)
        (planet taphao_thong)
        (planet taphao_kaew)
        (planet ursae_majoris_d)
        (planet dagon)
        (planet dimidium)
        (planet kepler_a)
        (planet kepler_b)
        (planet kepler_c)
        (planet kepler_d)
        (planet kepler_e)
        
        (radiation earth low_radiation)                     ; assign planets radiation levels
        (radiation venus low_radiation)
        (radiation mars low_radiation)
        (radiation jupiter high_radiation)
        (radiation saturn low_radiation)
        (radiation mercury low_radiation)
        (radiation uranus high_radiation)
        (radiation neptune low_radiation)
        (radiation proxima_b high_radiation)
        (radiation taphao_thong low_radiation)
        (radiation taphao_kaew low_radiation)
        (radiation ursae_majoris_d high_radiation)
        (radiation dagon high_radiation)
        (radiation dimidium low_radiation)
        (radiation kepler_a low_radiation)
        (radiation kepler_b low_radiation)
        (radiation kepler_c low_radiation)
        (radiation kepler_d high_radiation)
        (radiation kepler_e low_radiation)
        
        (damage not_damaged)                                ; initialise ship as not damaged
        (probe_damage probe_1 not_damaged)                  ; initialise probes as not damaged
        (probe_damage probe_2 not_damaged)
        (probe_damage probe_3 not_damaged)
        (probe_damage probe_4 not_damaged)
        (probe_damage probe_5 not_damaged)
        (lander_damage lander_1 not_damaged)                ; initialise landers as not damaged
        (lander_damage lander_2 not_damaged)
        (lander_damage lander_3 not_damaged)
        (lander_damage lander_4 not_damaged)
        (lander_damage lander_5 not_damaged)
        (lander_damage lander_6 not_damaged)
        (lander_damage lander_7 not_damaged)
        (lander_damage lander_8 not_damaged)
                                      
        (star_system proxima_centauri)			            ; assign space regions
        (star_system chalawan)
        (star_system sirius)
        (star_system betelgeuse)
        (star_system rigel)
        (star_system fomalhaut)
        (star_system vega)
        (star_system helvetios)
        (star_system kepler)
	    (star_system sol)
        
        (system_properties asteroid_belt proxima_centauri)  ; initialise properties
        (system_properties asteroid_belt betelgeuse)
        (system_properties asteroid_belt vega)
        (system_properties nebula sirius)
        (system_properties nebula proxima_centauri)
        (system_properties nebula helvetios)
        (system_properties nebula kepler)
        (system_properties nebula betelgeuse)
        (system_properties planet sol)
        (system_properties planet proxima_centauri)
        (system_properties planet chalawan)
        (system_properties planet fomalhaut)
        (system_properties planet helvetios)
        (system_properties planet kepler)

        (sample_exists plasma_sirius sirius)                ; plasma found in star systems
        (sample_exists plasma_proxima_centauri proxima_centauri)
        (sample_exists plasma_helvetios helvetios)
        (sample_exists plasma_kepler kepler)
        (sample_exists plasma_betelgeuse betelgeuse)
        
        (deep_space deep_space)                             ; initialise space
        
        (equipment MAV)                                     ; initialise equipment
        (equipment lander_1)
        (equipment lander_2)
        (equipment lander_3)
        (equipment lander_4)
        (equipment lander_5)
        (equipment lander_6)
        (equipment lander_7)
        (equipment lander_8)
        (equipment probe_1)
        (equipment probe_2)
        (equipment probe_3)
        (equipment probe_4)
        (equipment probe_5)
        (equipment radiation_shield)
        
        (equipment_location MAV launch_bay)                 ; initialise equipment locations
        (equipment_location lander_1 launch_bay)
        (equipment_location lander_2 launch_bay)
        (equipment_location lander_3 launch_bay)
        (equipment_location lander_4 launch_bay)
        (equipment_location lander_5 launch_bay)
        (equipment_location lander_6 launch_bay)
        (equipment_location lander_7 launch_bay)
        (equipment_location lander_8 launch_bay)
        (equipment_location probe_1 launch_bay)
        (equipment_location probe_2 launch_bay)
        (equipment_location probe_3 launch_bay)
        (equipment_location probe_4 launch_bay)
        (equipment_location probe_5 launch_bay)
        (equipment_location radiation_shield launch_bay)
        
        (personnel_location captain bridge)                 ; assign personnel to sections
        (personnel_location science_officer science_lab)
        (personnel_location navigator launch_bay)
        (personnel_location MAV_operator engineering)
        (personnel_location control_operator engineering)
        (personnel_location launch_controller engineering)
        (personnel_location engineer engineering)
        
        (planet_location earth sol)                         ; assign planets to their star_systems
        (planet_location venus sol)
        (planet_location mars sol)
        (planet_location jupiter sol)
        (planet_location saturn sol)
        (planet_location mercury sol)
        (planet_location uranus sol)
        (planet_location neptune sol)
        (planet_location proxima_b proxima_centauri)
        (planet_location taphao_thong chalawan)
        (planet_location taphao_kaew chalawan)
        (planet_location ursae_majoris_d chalawan)
        (planet_location dagon fomalhaut)
        (planet_location dimidium helvetios)
        (planet_location kepler_a kepler)
        (planet_location kepler_b kepler)
        (planet_location kepler_c kepler)
        (planet_location kepler_d kepler)
        (planet_location kepler_e kepler)
        
        (touchdown earth)                                   ; assign touchdown locations to planets
        (touchdown mars)
        (touchdown saturn)
        (touchdown uranus)
        (touchdown proxima_b)
        (touchdown taphao_kaew)
        (touchdown dagon)
        (touchdown kepler_a)
        (touchdown kepler_c)
        (touchdown kepler_e)
        
        (ship_location earth sol)                           ; start ship on Earth
        
        (optimal_touchdown earth found)
        
        (sections_adjacent bridge launch_bay)               ; assign layout of ship
        (sections_adjacent launch_bay bridge)
        (sections_adjacent launch_bay science_lab)
        (sections_adjacent science_lab launch_bay)
        (sections_adjacent launch_bay engineering)
        (sections_adjacent engineering launch_bay)
        (sections_adjacent launch_bay outside)
        (sections_adjacent outside launch_bay)

	    (planet_contains_life earth sol)                    ; assign planets that contain life
        (planet_contains_life mars sol)
        (planet_contains_life mercury sol)
        (planet_contains_life ursae_majoris_d chalawan)
        (planet_contains_life dagon fomalhaut)
        (planet_contains_life dimidium helvetios)
        (planet_contains_life kepler_a kepler)
        (planet_contains_life kepler_d kepler)

        (mission_control visit earth)                       ; things mission_control alread knows
        (mission_control earth alliance)
        (mission_control earth friendly)

        (task dagon war)                                    ; task to complete before headin back to sol
    )
    
    (:goal
        (and
            ; proves my war additional feature
            ; runs in 7.18 seconds using FF
            ; complete in 37 steps
            ; there is only one planet with no asteroid_belt, a touchdown location, and high radiation
            (mission_control dagon war)
        )
    )
)
