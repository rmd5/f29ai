﻿; F29AI Coursework 1
; Knowledge representation and automated planning
; Rory Dobson, rmd5

(define (domain space)
    (:requirements
        :strips :typing
    )
    
    (:types
        lander
        probe
	    planet_sys
        antenna
    )
    
    (:predicates
    
        ; ship layout and personnel
        (personnel ?personnel)                                          ; declare personnel, predefined
        (section_location ?section)                                     ; section on the ship, predefined
        (order_recieved ?personnel ?star_system)                        ; received an order to travel to star_system
        (personnel_location ?personnel_location ?section)               ; personnel location
        (sections_adjacent ?section1 ?section2)                         ; sections are next to eachother and can be travelled between, predefined
        
        ; equipment information
        (equipment_location ?equipment ?location)                       ; location of equipment on ship
        (equipment ?equipment)                                          ; declare equipment, predefined
        (damage ?damage)                                                ; damage of the ship
        (probe_damage ?probe ?damage)                                   ; damage of probes
        (operational ?equipment)                                        ; equipment is operational
        (deploy_antenna ?lander ?antenna)                               ; deploy antenna of lander
        (MAV_damage ?damage)                                            ; damage of the MAV
        
        ; space layout and properties
        (star_system ?star_system)                                      ; regions in space, predefined
        (planet ?planet)                                                ; declare planet, predefined
        (system_properties ?property ?star_system)                      ; nebula/asteroid belt/planet in the star_system, predefined
        (deep_space ?space)                                             ; declare deep space, predefined
        (planet_location ?planet ?star_system)                          ; planet is in star_system, predefined
        (ship_location ?place ?star_system)                             ; ship is located on planet/space in star_system
        (radiation ?planet ?level)                                      ; radiation level of planet, predefined
        
        ; planet scanning
        (touchdown ?planet)                                             ; planet has a touchdown location, predefined
        (optimal_touchdown ?planet ?found)                              ; planet's optimal touchdown is either found or not found
        (lander_damage ?lander ?damage)                                 ; level of damage to lander
        (lander_on_planet ?lander ?planet)                              ; lander successfully landed on planet
        (lander_scan ?planet ?level)                                    ; lander has scanned planet for radiation level
        
        ; sample recovery
        (sample_exists ?sample ?star_system)                            ; assign plasma type to system, predefined
        (sample ?sample ?star_system)                                   ; plasma sample from star_system or planet
        (sample_location ?plasma ?star_system ?section)                 ; plasma sample from star_system is at location on the ship
        (sample_studied ?plasma ?star_system)                           ; plasma sample from star_system has been studied
        
        ; probe scanning
        (planet_scan ?planet ?star_system)                              ; scan the planet in the star system
        
        ; databases
        (central_computer ?data1 ?data2)                                ; database of probe and lander scans
        (mission_control ?data1 ?data2)                                 ; information sent back to mission control

        ; additional functions
	    (planet_contains_life ?planet ?system)                          ; whether the planet contains life, predefined

        (task ?task ?system)                                            ; tasks needed to be complete before travel back to sol, predefined
    )
    
    ; give travel order to navigator, required before space_travel
    ; satisfies requirement 1
    (:action give_travel_order
        :parameters
            (?location_1 ?location_2)                                           ; locations from and to
        :precondition
            (and
                (or
                    (star_system ?location_2)                                   ; destination is either a system or a planet
                    (planet ?location_2)
                )
                (or 
                    (and                                                
                        (planet ?location_1)                                    ; in the even ?location_1 is a planet
                        (ship_location ?location_1 ?location_2)
                    )
                    (and                                                
                        (star_system ?location_1)                               ; in the event ?location_1 is a star_system
                        (ship_location deep_space ?location_1)
                    )
                )
                (not(exists(?x)
                    (order_recieved navigator ?x)                               ; no order already exists
                ))
                (personnel_location captain bridge)
                (personnel_location navigator bridge)
                (damage not_damaged)
                (equipment_location radiation_shield launch_bay)
                (not(operational MAV))
            )
        :effect
            (order_recieved navigator ?location_2)                              ; assign order to navigator for ?location_2
    )
    
    ; move personnel between locations on the ship
    ; satisfies requirement 3
    (:action move_personnel      
        :parameters
            (?personnel ?location_1 ?location_2)                                ; move personnel from ?location_1 to ?location_2
        :precondition
            (and
                (personnel ?personnel)
                (section_location ?location_1)
                (section_location ?location_2)
                (personnel_location ?personnel ?location_1)
                (sections_adjacent ?location_1 ?location_2)
            )
        :effect
            (and
                (not(personnel_location ?personnel ?location_1))                ; remove old personnel location
                (personnel_location ?personnel ?location_2)                     ; add new personnel location
            )
    )
    
    ; take off from a planet
    ; satisfies requirement 2
    ; satisfies addidional function 9
    (:action take_off
        :parameters
            (?planet ?system)
        :precondition
            (and
                (planet ?planet)
                (star_system ?system)
                (ship_location ?planet ?system)
                (planet_location ?planet ?system)
                (personnel_location captain bridge)
                (personnel_location navigator bridge)
                (order_recieved navigator ?system)
                (equipment_location radiation_shield launch_bay)
                (not(operational MAV))
            )
        :effect
            (and
                (not(ship_location ?planet ?system))                            ; remove previous ship location
                (ship_location deep_space ?system)                              ; assign new ship location
                (not(order_recieved navigator ?system))                         ; remove travel orders
                (when (central_computer ?planet war)                            ; if at war with the planet
                    (and
                        (not(damage not_damaged))                               ; ship becomes damaged due to attack from planet inhabitants
                        (damage damaged)
                    )
                )
            )
    )
    
    ; land ship on a planet
    ; satisfies addidional function 1
    (:action land_ship
        :parameters
            (?system ?planet)
        :precondition
            (and
                (planet ?planet)
                (star_system ?system)
                (ship_location deep_space ?system)
                (planet_location ?planet ?system)
                (optimal_touchdown ?planet found)
                (personnel_location captain bridge)
                (personnel_location navigator bridge)
                (order_recieved navigator ?planet)
                (equipment_location radiation_shield launch_bay)
                (not(operational MAV))
                (damage not_damaged)
            )
        :effect
            (and
                (not(ship_location deep_space ?system))                         ; remove previous ship location
                (ship_location ?planet ?system)                                 ; assign new ship location
                (not(order_recieved navigator ?planet))                         ; remove travel orders
                (central_computer visit ?planet)                                ; log visit to planet
            )
    
    )
    
    ; travel between different regions of space
    ; satisfies requirement 2 and 13
    (:action space_travel
        :parameters
            (?system_1 ?system_2)
        :precondition
            (and
                (ship_location deep_space ?system_1)
                (personnel_location captain bridge)
                (personnel_location navigator bridge)
                (order_recieved navigator ?system_2)
                (damage not_damaged)
                (equipment_location radiation_shield launch_bay)
                (not(operational MAV))
		        (damage not_damaged)
                (not(central_computer visit ?system_2))
                (or                                                             ; while there are tasks still to complete, does not return to sol
                    (and
                        (exists (?x ?y)
                            (task ?x ?y)
                        )
                        (not(= ?system_2 sol))
                    )
                    (not(exists (?x ?y)
                        (task ?x ?y)
                    ))
                )
       	    )
        :effect
            (and
                (when (system_properties asteroid_belt ?system_2)               ; if system ?system_2 contains asteroid belt, ship is damaged
                    (and                                                        ; satisfies requirement 13
                        (not(damage not_damaged))                               ; ship becomes damaged
                        (damage damaged)
                    )
                )
                (not(ship_location deep_space ?system_1))                       ; remove previous ship location
                (ship_location deep_space ?system_2)                            ; assign new ship location
                (not(order_recieved navigator ?system_2))                       ; remove travel orders
		        (central_computer visit ?system_2)                              ; log visit into the central_computer
            )
    )
    
    ; power up the MAV, move MAV_operator into it
    (:action power_up_MAV
        :parameters
            ()
        :precondition
            (and
                (personnel_location MAV_operator launch_bay)
                (personnel_location control_operator engineering)
                (equipment MAV)
                (equipment_location MAV launch_bay)
                (not(MAV_damage damaged))
            )
        :effect
            (and
                (not(personnel_location MAV_operator launch_bay))               ; MAV_operator is no longer in the launch_bay
                (personnel_location MAV_operator MAV)                           ; MAV_operator is in the MAV
                (operational MAV)                                               ; the MAV is operational
            )
    )
    
    ; power down the MAV, move MAV_operator out of it
    (:action power_down_MAV
        :parameters
            ()
        :precondition
            (and
                (operational MAV)
                (equipment_location MAV launch_bay)
                (damage not_damaged)
            )
        :effect
            (and
                (not(personnel_location MAV_operator MAV))                      ; MAV_operator is no longer in the MAV
                (personnel_location MAV_operator launch_bay)                    ; MAV_operator is in the launch_bay
                (not(operational MAV))                                          ; MAV is no longer operational
            )
    )

    ; disable the MAV if it is caught outside without the radiation_shield
    ; satisfies requirement 14
    (:action disable_MAV
        :parameters
            ()
        :precondition
            (and
                (equipment_location MAV outside)
                (not(equipment_location radiation_shield outside))
            )
        :effect
            (MAV_damage damaged)
    )
    
    ; extravehicular activity to fix the ship
    ; satisfies requirement 4
    (:action EVA_fix_damage
        :parameters
            (?system)
        :precondition
            (and
                (damage damaged)                                                ; ship is damaged, required to be fixed
                (operational MAV)
                (star_system ?system)
                (ship_location deep_space ?system)
                (personnel_location MAV_operator MAV)
                (personnel_location control_operator engineering)
                (equipment_location MAV outside)
                (or
                    (not(system_properties nebula ?system))                     ; either the system does not conatains a nebula
                    (equipment_location radiation_shield outside)               ; or the radiation shield is outside to prevent the MAV from failing
                )
            )
        :effect
            (and
                (not(damage damaged))                                           ; ship is no longer damaged
                (damage not_damaged)                                            ; ship is not_damaged
            )
    )
    
    ; move equipment between ship sections
    (:action move_equipment
        :parameters
            (?equipment ?location_1 ?location_2)
        :precondition
            (and
                (equipment_location ?equipment ?location_1)
                (personnel_location launch_controller launch_bay)
                (sections_adjacent ?location_1 ?location_2)
                (or
                    (not(= ?equipment MAV))                                     ; if the equipment is the MAV then the radiation shield must be outside
                    (and
                        (= ?equipment MAV)
                        (equipment_location radiation_shield outside)
                        (operational MAV)
                    )
                )
            )
        :effect
            (and
                (not(equipment_location ?equipment ?location_1))                ; equipment no longer at ?location_1
                (equipment_location ?equipment ?location_2)                     ; equipment located at ?location_2
            )
    )
    
    ; deploy probe to a nebula to obtain sample of plasma
    ; satisfies requirement 6 and 8
    (:action deploy_probe_to_nebula
        :parameters
            (?sample ?probe ?system)
        :precondition
            (and
                (equipment_location ?probe launch_bay)
                (ship_location deep_space ?system)
                (system_properties nebula ?system)                              ; star_system contains a nebula
                (probe_damage ?probe not_damaged)
                (personnel_location launch_controller launch_bay)
                (sample_exists ?sample ?system)
            )
        :effect
            (and
                (when (not(system_properties asteroid_belt ?system))            ; when there's not an asteroid_belt
                    (and
                        (sample ?sample ?system)                                ; sample the plasma from the star_system
                        (sample_location ?sample ?system launch_bay)            ; sample location is in the launch_bay       
                    )
                )
                (when (system_properties asteroid_belt ?system)                 ; when there is an asteroid_belt
                    (and
                        (not(probe_damage ?probe not_damaged))                  ; probe is no longer not_damaged
                        (probe_damage ?probe destroyed)                         ; probe is destroyed
			            (central_computer ?probe destroyed)                     ; log probe destruction in central_computer
                        (not(task ?probe destroyed))                            ; check off task
                    )
                )
            )
    )
    
    ; science_officer needs to collect the plasma from the launch_bay, as they are the only ones that can handle it
    ; satisfies requirement 9
    (:action science_officer_collect_sample
        :parameters
            (?sample ?system)
        :precondition
            (and
                (personnel_location science_officer launch_bay)
		        (sample ?sample ?system)                                        ; the plasma has been sampled
                (sample_location ?sample ?system launch_bay)                    ; the plasma is in the launch_bay
		        (ship_location deep_space ?system)
            )
        :effect
            (and
                (not(sample_location ?sample ?system launch_bay))               ; the plasma sample is no longer in the launch_bay
                (sample_location ?sample ?system science_officer)               ; the science_officer has the plasma
            )
    )
    
    ; the science_officer must study the plasma sample before logging it into the central_computer
    (:action study_sample
        :parameters
            (?sample ?location)
        :precondition
            (and
                (sample_location ?sample ?location science_officer)             ; the science_officer has the plasma
                (personnel_location science_officer science_lab)                ; the science_officer is in the science_lab
                (ship_location deep_space ?location)
                (sample ?sample ?location)
                (sample_exists ?sample ?location)
            )
        :effect
            (and
                (sample_studied ?sample studied)                                ; the plasma sample has been studied
                (central_computer ?sample studied)                              ; the study is logged on the central_computer
                (not(sample_location ?sample ?location science_officer))        ; the science_officer no longer has the sample
                (sample_location ?sample ?location science_lab)                 ; the sample is in the science_lab
                (not(task ?sample studied))
            )
    )
    
    ; deploy the probe to scan the planet for an optimal_touchdown location
    ; satisfies requirement 7 and 8
    (:action deploy_probe_to_planet
        :parameters
            (?probe ?planet ?system)
        :precondition
            (and
                (equipment_location ?probe launch_bay)
                (ship_location deep_space ?system)
                (system_properties planet ?system)
                (planet_location ?planet ?system)
                (probe_damage ?probe not_damaged)
                (personnel_location launch_controller launch_bay)
                (central_computer visit ?system)
            )
        :effect
            (and
                (when (not(system_properties asteroid_belt ?system))            ; when there is not an asteroid_belt
                    (and
                        (planet_scan ?planet ?system)                           ; the planet has been scanned
                        (central_computer ?planet ?system)                      ; the scan is logged
                    )
                )
                (when (system_properties asteroid_belt ?system)                 ; when there is an asteroid_belt
                    (and
                        (not(probe_damage ?probe not_damaged))                  ; the probe is no longer not_damaged
                        (probe_damage ?probe destroyed)                         ; the probe is destroyed
			            (central_computer ?probe destroyed)                     ; the destroyed probe is logged onto the central_computer
                        (not(task ?probe destroyed))
                    )
                )
            )
    )
    
    ; optimal touchdown location must be searched for before a lander is deployed
    (:action search_for_optimal_touchdown
        :parameters
            (?planet ?system)
        :precondition
            (and
                (ship_location deep_space ?system)
                (planet_scan ?planet ?system)
                (or
                    (touchdown ?planet)
                    (not(touchdown ?planet))
                )
            )
        :effect
            (and
                (when (not(touchdown ?planet))                                  ; determine if the optimal touchdown location is found
                    (optimal_touchdown ?planet not_found)
                )
                (when (touchdown ?planet)
                    (optimal_touchdown ?planet found)
                )
            )
    )
    
    ; deploy a lander to the planet
    ; satisfies requirement 10
    (:action deploy_lander
        :parameters
            (?lander - lander ?planet ?system)
        :precondition
            (and
                (planet ?planet)
                (star_system ?system)
                (planet_location ?planet ?system)
                (or
                    (optimal_touchdown ?planet found)
                    (optimal_touchdown ?planet not_found)
                )
                (ship_location deep_space ?system)
                (equipment_location ?lander launch_bay)
                (personnel_location launch_controller launch_bay)
                (lander_damage ?lander not_damaged)
            )
        :effect
            (and
                (when (optimal_touchdown ?planet found)                         ; if there is an optimal_touchdown, lander lands on planet
                    (and
                        (lander_on_planet ?lander ?planet)
                        (not(equipment_location ?lander launch_bay))
                        (equipment_location ?lander ?planet)
                    )
                )
                (when (optimal_touchdown ?planet not_found)                     ; if there is not an optimal_touchdown, lander destroyed
                    (and
                        (not (lander_damage ?lander not_damaged))
                        (lander_damage ?lander destroyed)
                        (not(equipment_location ?lander launch_bay))
                        (equipment_location ?lander ?planet)
			            (central_computer ?lander destroyed)                    ; log lander destruction
                        (not(task ?lander destroyed))                           ; check off task
                    )
                )
            )
    )
    
    ; the lander scans the planet for radioactivity
    ; satisfies requirement 11
    (:action lander_scan
        :parameters
            (?lander - lander ?planet ?system ?radiation)
        :precondition
            (and
                (equipment ?lander)
                (planet ?planet)
                (radiation ?planet ?radiation)
                (equipment_location ?lander ?planet)
                (lander_on_planet ?lander ?planet)
                (lander_damage ?lander not_damaged)
                (planet_location ?planet ?system)
                (ship_location deep_space ?system)
            )
        :effect
            (and
                (lander_scan ?planet ?radiation)                            ; lander scans planet for radiation_level
            )
    )
    
    ; lander deploys antenna to relay scan information back to the ship, must deploy two if planet has high radiation
    ; satisfies requirement 12
    (:action lander_deploy_antenna
        :parameters
            (?lander - lander ?planet ?system ?radiation)
        :precondition
            (and
                (equipment ?lander)
                (planet ?planet)
                (equipment_location ?lander ?planet)
                (lander_on_planet ?lander ?planet)
                (lander_damage ?lander not_damaged)
                (planet_location ?planet ?system)
                (ship_location deep_space ?system)
                (lander_scan ?planet ?radiation)
                (radiation ?planet ?radiation)
            )
        :effect
            (and
                (when (= ?radiation low_radiation)                          ; if there is a low level of radiation, only deploy one antenna
                    (deploy_antenna ?lander antenna_1)
                )
                (when (= ?radiation high_radiation)                         ; if there is a high level of radiation, deploy two antennas
                    (and
                        (deploy_antenna ?lander antenna_1)
                        (deploy_antenna ?lander antenna_2)
                    )
                )
            )
    )
    
    ; lander transmits planet information back to the ship and it is logged in the central computer
    ; satisfies requirement 12
    (:action lander_transmission
        :parameters
            (?lander - lander ?planet ?system ?radiation)
        :precondition
            (and
                (equipment ?lander)
                (planet ?planet)
                (equipment_location ?lander ?planet)
                (lander_on_planet ?lander ?planet)
                (lander_damage ?lander not_damaged)
                (planet_location ?planet ?system)
                (ship_location deep_space ?system)
                (lander_scan ?planet ?radiation)
                (radiation ?planet ?radiation)
                (or                                                         ; either on antenna in low level radiation
                    (and
                        (= ?radiation low_radiation)
                        (deploy_antenna ?lander antenna_1)
                    )
                    (and                                                    ; or two antennas in high radiation
                        (= ?radiation high_radiation)
                        (deploy_antenna ?lander antenna_1)
                        (deploy_antenna ?lander antenna_2)
                    )
                )
            )
        :effect
            (and
                (central_computer ?planet ?radiation)                       ; store the result of the lander scan
                (not(task ?planet ?radiation))                              ; check off task
            )
    )
    
    ; send data acquired from the central computer to mission control, provided the ship is on earth
    ; satisfies requirement 15
    (:action report_to_mission_control
        :parameters
            (?d1 ?d2)
        :precondition
            (and
                (ship_location earth sol)
                (central_computer ?d1 ?d2)
            )
        :effect
            (and
                (mission_control ?d1 ?d2)                                   ; transfer data to mission control
                (not(central_computer ?d1 ?d2))                             ; delete from central_computer after transfer
            )
    )
    

    ; my own implementations, which involve discovering life, determining the disposition of the lifeforms,
    ; and either creating an alliance or starting a war
    ; if an alliance is created, planet_resources are obtained and collected by the captain
    ; if a war is started, the ship is attacked and damaged while leaving the planet


    ; ship must land on the planet in order to discover any life there, discovery is then logged on the central computer
    ; satisfies addidional function 2
    (:action discover_life
        :parameters
            (?planet ?system ?radiation)
        :precondition
            (and
                (radiation ?planet ?radiation)
                (central_computer ?planet ?radiation)
                (planet_location ?planet ?system)
                (ship_location ?planet ?system)
                (planet_contains_life ?planet ?system)
            )
        :effect
            (and
                (central_computer ?planet life)                             ; log the discovery of life on the planet
            )
    )
    
    ; the MAV is deployed to meet the life on the planet and determines if they are friend or foe, depending on the radioactivity
    ; satisfies addidional function 3
    ; satisfies addidional function 4
    (:action determine_disposition
        :parameters
            (?planet ?system ?radiation)
        :precondition
            (and
		        (radiation ?planet ?radiation)
                (central_computer ?planet ?radiation)
		        (central_computer ?planet life)
                (planet_location ?planet ?system)
                (ship_location ?planet ?system)
                (equipment_location MAV outside)
            )
        :effect
            (and
                (when (= ?radiation high_radiation)                         ; when the radiation is high the lifeforms are hostile
                    (central_computer ?planet hostile)
                )
                (when (= ?radiation low_radiation)                          ; when the radiation is low the lifeforms are friendly
                    (central_computer ?planet friendly)
                )
            )
    )
    
    ; if the planet is friendly, an alliance is created and the ship is gifted some resources from the planet
    ; satisfies addidional function 5
    ; satisfies addidional function 9
    (:action create_alliance
        :parameters
            (?planet ?system)
        :precondition
            (and
                (planet_location ?planet ?system)
                (ship_location ?planet ?system)
                (planet_contains_life ?planet ?system)
                (equipment_location MAV outside)
                (operational MAV)
                (central_computer ?planet friendly)
            )
        :effect
            (and
                (central_computer ?planet alliance)                         ; log alliance with planet
                (sample planet_resources ?planet)                           ; sample resources obtained
                (sample_location planet_resources ?planet launch_bay)       ; transfer resources to launch_bay
                (not(task ?planet alliance))                                ; check off task
            )
    )

    ; captain must collect the sample
    ; satisfies addidional function 6
    ; satisfies addidional function 9
    (:action captain_collect_planet_sample
        :parameters
            (?planet ?system)
        :precondition
            (and
                (personnel_location captain launch_bay)
		        (sample planet_resources ?planet)
                (sample_location planet_resources ?planet launch_bay)
		        (ship_location ?planet ?system)
            )
        :effect
            (and
                (not(sample_location planet_resources ?planet launch_bay))  ; resources no longer in launch_bay
                (sample_location planet_resources ?planet captain)          ; captain has the resources
                (not(task planet_resources ?planet))                        ; check off task
                (central_computer planet_resources ?planet)                 ; log resources into central_computer
            )
    )
    
    ; if the life on the planet is hostile, then a war is started and the ship is damaged upon leaving the planet
    ; satisfies addidional function 7
    ; satisfies addidional function 9
    (:action start_war
        :parameters
            (?planet ?system)
        :precondition
            (and
                (planet_location ?planet ?system)
                (ship_location ?planet ?system)
                (planet_contains_life ?planet ?system)
                (equipment_location MAV outside)
                (operational MAV)
                (central_computer ?planet hostile)
            )
        :effect
            (and
                (central_computer ?planet war)                              ; log war with planet
                (not(task ?planet war))                                     ; check off task
            )
    )
)